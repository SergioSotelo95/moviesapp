package com.example.moviesapp.data.model

data class DetailsModel(
    val seasons: List<SeasonModel>
)