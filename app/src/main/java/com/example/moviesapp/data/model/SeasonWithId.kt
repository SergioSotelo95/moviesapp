package com.example.moviesapp.data.model

data class SeasonWithId (
    val showId: Int,
    val episodeCount: Int,
    val id: Int,
    val name: String,
    val overview: String,
    val posterPath: String?,
    val seasonNumber: Int
        )