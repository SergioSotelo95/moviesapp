package com.example.moviesapp.data.model.authentication

data class SessionModel(
    val success: Boolean,
    val session_id: String
)
