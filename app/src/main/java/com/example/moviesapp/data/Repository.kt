package com.example.moviesapp.data

import com.example.moviesapp.data.model.EpisodesResponseModel
import com.example.moviesapp.data.model.ResultModel
import com.example.moviesapp.data.model.SeasonWithId
import com.example.moviesapp.data.model.authentication.RequestTokenModel
import com.example.moviesapp.data.model.authentication.SessionModel
import com.example.moviesapp.data.model.authentication.UserModel

interface Repository {
    suspend fun getPopular(page: Int?): ResultModel

    suspend fun getTopRated(page: Int?): ResultModel

    suspend fun getOnAir(page: Int?): ResultModel

    suspend fun getAiringToday(page: Int?): ResultModel

    suspend fun getDetails(id: Int): List<SeasonWithId>

    suspend fun getEpisodes(id: Int, season: Int): EpisodesResponseModel

    suspend fun getRequestToken(): RequestTokenModel?

    suspend fun sendCredentials(username: String, password: String, requestToken: String): RequestTokenModel?

    suspend fun createSession(requestToken: String): SessionModel?

    suspend fun getUser(sessionId: String): UserModel

    suspend fun getFavoriteShows(sessionId: String): ResultModel

}