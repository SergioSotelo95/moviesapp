package com.example.moviesapp.data.network


import com.example.moviesapp.data.model.DetailsModel
import com.example.moviesapp.data.model.EpisodesResponseModel
import com.example.moviesapp.data.model.ResultModel
import com.example.moviesapp.data.model.authentication.RequestTokenModel
import com.example.moviesapp.data.model.authentication.SessionModel
import com.example.moviesapp.data.model.authentication.UserModel
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface ShowsApiClient {

    @GET("3/tv/airing_today?api_key=803c16c6978a5b853d8afc36950f35cb&language=en-US}")
    fun getAiringToday(
        @Query("page") page: Int? = 1
    ): Call <ResultModel>

    @GET("3/tv/on_the_air?api_key=803c16c6978a5b853d8afc36950f35cb&language=en-US")
    fun getOnTheAir(
        @Query("page") page: Int? = 1
    ): Call <ResultModel>

    @GET("3/tv/popular?api_key=803c16c6978a5b853d8afc36950f35cb&language=en-US")
    fun getPopular(
        @Query("page") page: Int? = 1
    ): Call <ResultModel>

    @GET("3/tv/top_rated?api_key=803c16c6978a5b853d8afc36950f35cb&language=en-US")
    fun getTopRated(
        @Query("page") page: Int? = 1
    ): Call <ResultModel>

    @GET("3/tv/{id}?api_key=803c16c6978a5b853d8afc36950f35cb&language=en-US")
    fun getDetails(
        @Path("id") id: Int
    ): Call <DetailsModel>

    @GET("3/tv/{id}/season/{season}?api_key=803c16c6978a5b853d8afc36950f35cb")
    fun getEpisodes(
        @Path("id") id: Int,
        @Path("season") season: Int
    ): Call<EpisodesResponseModel>

    @GET("3/authentication/token/new?api_key=803c16c6978a5b853d8afc36950f35cb")
    suspend fun getRequestToken(): Response<RequestTokenModel>

    @FormUrlEncoded
    @POST("3/authentication/token/validate_with_login?api_key=803c16c6978a5b853d8afc36950f35cb")
    suspend fun sendCredentials(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("request_token") requestToken: String
    ): Response<RequestTokenModel>

    @FormUrlEncoded
    @POST("3/authentication/session/new?api_key=803c16c6978a5b853d8afc36950f35cb")
    suspend fun createSession(
        @Field("request_token") requestToken: String
    ): Response<SessionModel>

    @GET("3/account?api_key=803c16c6978a5b853d8afc36950f35cb")
    fun getUser(
        @Query("session_id") sessionId: String
    ): Call<UserModel>

    @GET("3/account/%7Baccount_id%7D/favorite/tv?api_key=803c16c6978a5b853d8afc36950f35cb")
    fun getFavorites(
        @Query("session_id") sessionId: String
    ): Call<ResultModel>

}