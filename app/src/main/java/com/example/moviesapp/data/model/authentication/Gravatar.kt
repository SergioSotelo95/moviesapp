package com.example.moviesapp.data.model.authentication

data class Gravatar(
    val hash: String
)