package com.example.moviesapp.data.model.authentication

data class Tmdb(
    val avatar_path: String?
)