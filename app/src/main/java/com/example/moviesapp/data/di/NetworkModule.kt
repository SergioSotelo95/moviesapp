package com.example.moviesapp.data.di

import android.content.Context
import androidx.room.Room
import com.example.moviesapp.data.database.ShowsDatabase
import com.example.moviesapp.data.network.ShowsApiClient
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    @Singleton
    @Provides
    fun provideShowsApiClient(retrofit: Retrofit): ShowsApiClient {
        return retrofit.create(ShowsApiClient::class.java)
    }

    @Singleton
    @Provides
    fun provideShowsDatabase(@ApplicationContext context: Context) = Room
        .databaseBuilder(context.applicationContext, ShowsDatabase::class.java, "shows")
}