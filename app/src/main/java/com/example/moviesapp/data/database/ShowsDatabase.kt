package com.example.moviesapp.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.moviesapp.data.model.ShowModel

@Database(
    entities = [ShowModel::class],
    version = 1,
    exportSchema = false
)
abstract class ShowsDatabase : RoomDatabase() {
//    abstract fun showsDao()
}