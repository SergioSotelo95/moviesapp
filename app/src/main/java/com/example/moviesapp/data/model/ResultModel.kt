package com.example.moviesapp.data.model

import com.google.gson.annotations.SerializedName

data class ResultModel(
    @SerializedName("page")
    val page: Int,
    @SerializedName("results")
    val results: List<ShowModel>,
    @SerializedName("total_pages")
    val totalPages: Int,
    @SerializedName("total_results")
    val totalResults: Int
)