package com.example.moviesapp.data.model.authentication

data class RequestTokenModel(
    val expires_at: String,
    val request_token: String,
    val success: Boolean
)