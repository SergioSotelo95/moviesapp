package com.example.moviesapp.data.model.authentication

data class UserModel(
    val avatar: Avatar,
    val id: Int,
    val name: String,
    val username: String
)