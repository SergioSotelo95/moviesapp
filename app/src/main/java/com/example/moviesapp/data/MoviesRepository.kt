package com.example.moviesapp.data

import com.example.moviesapp.data.model.*
import com.example.moviesapp.data.model.authentication.RequestTokenModel
import com.example.moviesapp.data.model.authentication.SessionModel
import com.example.moviesapp.data.model.authentication.UserModel
import com.example.moviesapp.data.network.ShowsApiClient
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.await
import javax.inject.Inject

class MoviesRepository @Inject constructor(
    private val api: ShowsApiClient
) : Repository {
    override suspend fun getPopular(page: Int?): ResultModel {
        return withContext(Dispatchers.IO) {
            val responseAPI = api.getPopular().await()
            responseAPI
        }
    }


    override suspend fun getTopRated(page: Int?): ResultModel {
        return withContext(Dispatchers.IO) {
            val responseAPI = api.getTopRated().await()
            responseAPI
        }
    }

    override suspend fun getOnAir(page: Int?): ResultModel {
        return withContext(Dispatchers.IO) {
            val responseAPI = api.getOnTheAir().await()
            responseAPI
        }
    }

    override suspend fun getAiringToday(page: Int?): ResultModel {
        return withContext(Dispatchers.IO) {
            val responseAPI = api.getAiringToday().await()
            responseAPI
        }
    }

    override suspend fun getDetails(id: Int): List<SeasonWithId> {
        return withContext(Dispatchers.IO) {
            val responseAPI = api.getDetails(id).await().seasons
            responseAPI.map {
                it.seasonToSeasonId(id)
            }
        }
    }
    fun SeasonModel.seasonToSeasonId(id: Int): SeasonWithId {
        return SeasonWithId(
            id = this.id,
            episodeCount = this.episodeCount,
            seasonNumber = this.seasonNumber,
            overview = this.overview,
            name = this.name,
            posterPath = this.posterPath,
            showId = id
        )
    }

    override suspend fun getEpisodes(id: Int, season: Int): EpisodesResponseModel {
        return withContext(Dispatchers.IO) {
            val responseAPI = api.getEpisodes(id, season).await()
            responseAPI
        }
    }

    override suspend fun getRequestToken(): RequestTokenModel? {
        return withContext(Dispatchers.IO) {
            val responseAPI = api.getRequestToken()
            responseAPI.body()
        }
    }

    override suspend fun sendCredentials(username: String, password: String, requestToken: String): RequestTokenModel? {
        return withContext(Dispatchers.IO) {
            val responseAPI = api.sendCredentials(username, password, requestToken)
            responseAPI.body()
        }
    }

    override suspend fun createSession(requestToken: String): SessionModel? {
        return withContext(Dispatchers.IO) {
            val responseAPI = api.createSession(requestToken)
            responseAPI.body()
        }
    }

    override suspend fun getUser(sessionId: String): UserModel {
        return withContext(Dispatchers.IO) {
            val responseAPI = api.getUser(sessionId).await()
            responseAPI
        }
    }

    override suspend fun getFavoriteShows(sessionId: String): ResultModel {
        return withContext(Dispatchers.IO) {
            val responseAPI = api.getFavorites(sessionId).await()
            responseAPI
        }
    }

    suspend fun session (username: String, password: String): SessionModel? {
        val token = getRequestToken()
        return if(token?.success == true) {
            val tokenOk = sendCredentials(username, password, token.request_token)
            if (tokenOk != null){
                createSession(tokenOk.request_token)
            } else { null }

        } else {
            null
        }

    }


}