package com.example.moviesapp.data.model.authentication

data class Avatar(
    val gravatar: Gravatar,
    val tmdb: Tmdb
)