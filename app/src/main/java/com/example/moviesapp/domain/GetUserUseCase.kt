package com.example.moviesapp.domain

import com.example.moviesapp.data.MoviesRepository
import com.example.moviesapp.data.model.authentication.UserModel
import javax.inject.Inject

class GetUserUseCase @Inject constructor(private val repository: MoviesRepository) {
    suspend operator fun invoke(sessionId: String): UserModel =
        repository.getUser(sessionId)
}