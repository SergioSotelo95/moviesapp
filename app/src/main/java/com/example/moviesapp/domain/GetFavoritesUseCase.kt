package com.example.moviesapp.domain

import com.example.moviesapp.data.MoviesRepository
import com.example.moviesapp.data.model.ResultModel
import javax.inject.Inject

class GetFavoritesUseCase @Inject constructor(private val repository: MoviesRepository) {
    suspend operator fun invoke(sessionId: String): ResultModel =
        repository.getFavoriteShows(sessionId)
}