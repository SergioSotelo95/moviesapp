package com.example.moviesapp.domain

import com.example.moviesapp.data.MoviesRepository
import com.example.moviesapp.data.model.EpisodesResponseModel
import javax.inject.Inject

class GetEpisodesUseCase @Inject constructor(private val repository: MoviesRepository) {
    suspend operator fun invoke(page: Int, season: Int): EpisodesResponseModel =
        repository.getEpisodes(page, season)
}