package com.example.moviesapp.domain

import com.example.moviesapp.data.MoviesRepository
import com.example.moviesapp.data.model.authentication.SessionModel
import javax.inject.Inject

class GetLoginSessionUseCase @Inject constructor(private val repository: MoviesRepository){
    suspend operator fun invoke(username:String, password:String): SessionModel? =
        repository.session(username, password)
}