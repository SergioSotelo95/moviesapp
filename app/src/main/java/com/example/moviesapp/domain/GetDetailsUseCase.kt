package com.example.moviesapp.domain

import com.example.moviesapp.data.MoviesRepository
import com.example.moviesapp.data.model.SeasonWithId
import javax.inject.Inject

class GetDetailsUseCase @Inject constructor(private val repository: MoviesRepository) {
    suspend operator fun invoke(id: Int): List<SeasonWithId> =
        repository.getDetails(id)
}