package com.example.moviesapp.domain

import com.example.moviesapp.data.MoviesRepository
import com.example.moviesapp.data.model.ResultModel
import javax.inject.Inject

class GetPopularUseCase @Inject constructor(private val repository: MoviesRepository) {
    suspend operator fun invoke(page: Int? = 1): ResultModel =
        repository.getPopular(page)
}