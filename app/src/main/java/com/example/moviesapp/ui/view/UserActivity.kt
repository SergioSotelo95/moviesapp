package com.example.moviesapp.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.moviesapp.R
import com.example.moviesapp.databinding.ActivityUserBinding
import com.example.moviesapp.ui.adapter.ShowListAdapter
import com.example.moviesapp.ui.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UserActivity : AppCompatActivity() {

    private lateinit var binding: ActivityUserBinding
    private val viewModel: UserViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {

        val sessionId = intent.getStringExtra("sessionId")

        super.onCreate(savedInstanceState)
        binding = ActivityUserBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupRecyclerView()

        viewModel.getUser(sessionId!!)
        viewModel.getFavorites(sessionId)

        viewModel.userInfo.observe(this) {
            binding.UserName.text = it.username

            val pictureUrl =
                "https://secure.gravatar.com/avatar/" + it.avatar.gravatar.hash + ".jpg?s=300"

            Glide.with(binding.UserLL.context).load(pictureUrl).placeholder(R.drawable.error)
                .into(binding.UserPicture)
        }

        viewModel.userFavorites.observe(this) {
            viewModel.getFavorites(sessionId)
            (binding.UserRV.adapter as ShowListAdapter).submitList(it)
        }

    }
    private fun setupRecyclerView() = binding.UserRV.apply {
        adapter = ShowListAdapter()
        layoutManager = LinearLayoutManager(this@UserActivity, RecyclerView.HORIZONTAL, false)
    }
}