package com.example.moviesapp.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.moviesapp.R
import com.example.moviesapp.databinding.ActivitySeasonBinding
import com.example.moviesapp.ui.adapter.SeasonListAdapter
import com.example.moviesapp.ui.viewmodel.SeasonsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SeasonActivity : AppCompatActivity() {


    private lateinit var binding: ActivitySeasonBinding
    private val viewModel: SeasonsViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        val title = intent.getStringExtra("title")
        val overView = intent.getStringExtra("overView")
        val posterURL = "https://image.tmdb.org/t/p/w500" + intent.getStringExtra("poster")
        val showId = intent.getIntExtra("id", 0)

        super.onCreate(savedInstanceState)
        binding = ActivitySeasonBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupRecyclerView()

        viewModel.getResults(showId)

        viewModel.seasonList.observe(this){
            (binding.DetailsRV.adapter as SeasonListAdapter).submitList(it)
        }

        binding.DetailsTitle.text = title

        binding.DetailsOverview.text = overView

        Glide.with(binding.DetailsLL.context).load(posterURL).placeholder(R.drawable.error)
            .into(binding.DetailsPoster)


    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.favorite, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.FavoriteOn -> {
                Toast.makeText(this, "Show added to favorites.", Toast.LENGTH_SHORT).show()
                true
            } else -> {
                false
            }
        }
    }

    private fun setupRecyclerView() = binding.DetailsRV.apply {
        adapter = SeasonListAdapter()
        layoutManager = LinearLayoutManager(this@SeasonActivity)
    }
}