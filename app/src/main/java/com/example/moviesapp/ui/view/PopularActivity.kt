package com.example.moviesapp.ui.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.moviesapp.R
import com.example.moviesapp.databinding.ActivityPopularBinding
import com.example.moviesapp.ui.adapter.ShowListAdapter
import com.example.moviesapp.ui.viewmodel.PopularListViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PopularActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPopularBinding
    private val viewModel: PopularListViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        val sessionId = intent.getStringExtra("sessionId")

        super.onCreate(savedInstanceState)
        binding = ActivityPopularBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupRecyclerView()

        viewModel.showList.observe(this) {
            (binding.RV.adapter as ShowListAdapter).submitList(it)
        }

        binding.ButtonAiringToday.setOnClickListener {
            val intentAiringToday = Intent(binding.RV.context, AiringTodayActivity::class.java)
            intentAiringToday.putExtra("sessionId", sessionId)
            ContextCompat.startActivity(binding.RV.context, intentAiringToday, null)
        }
        binding.ButtonOnAir.setOnClickListener {
            val intentOnAir = Intent(binding.RV.context, OnAirActivity::class.java)
            intentOnAir.putExtra("sessionId", sessionId)
            ContextCompat.startActivity(binding.RV.context, intentOnAir, null)

        }
        binding.ButtonTopRated.setOnClickListener {
            val intentTopRated = Intent(binding.RV.context, TopRatedActivity::class.java)
            intentTopRated.putExtra("sessionId", sessionId)
            ContextCompat.startActivity(binding.RV.context, intentTopRated, null)

        }

    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val sessionId = intent.getStringExtra("sessionId")
        return when(item.itemId){
            R.id.Profile -> {
                val intentProfile = Intent(this, UserActivity::class.java)
                intentProfile.putExtra("sessionId", sessionId)
                ContextCompat.startActivity(this, intentProfile, null)
                true
            } else -> {
                false
            }
        }
    }
    override fun onBackPressed() {
        super.onBackPressed()
        moveTaskToBack(true)
    }
    private fun setupRecyclerView() = binding.RV.apply {
        adapter = ShowListAdapter()
        layoutManager = GridLayoutManager(this@PopularActivity, 2, RecyclerView.VERTICAL, false)
    }
}
