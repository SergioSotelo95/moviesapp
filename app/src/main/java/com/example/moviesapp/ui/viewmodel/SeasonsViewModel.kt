package com.example.moviesapp.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.moviesapp.data.model.SeasonWithId
import com.example.moviesapp.domain.GetDetailsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SeasonsViewModel @Inject constructor(
    application: Application,
    private val getDetailsUseCase: GetDetailsUseCase
) : AndroidViewModel(application) {
    val seasonList = MutableLiveData<List<SeasonWithId>>()

    fun getResults(id: Int) {
        viewModelScope.launch() {
            val result = getDetailsUseCase(id)
            if (result.isNotEmpty()){
                seasonList.postValue(result)
            }
        }
    }

}