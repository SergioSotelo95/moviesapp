package com.example.moviesapp.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moviesapp.databinding.ActivityEpisodesBinding
import com.example.moviesapp.ui.adapter.EpisodesListAdapter
import com.example.moviesapp.ui.viewmodel.EpisodesViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class EpisodesActivity : AppCompatActivity() {

    private lateinit var binding: ActivityEpisodesBinding
    private val viewModel: EpisodesViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        val showId = intent.getIntExtra("showId", 0)
        val seasonNumber = intent.getIntExtra("season", 0)

        super.onCreate(savedInstanceState)
        binding = ActivityEpisodesBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupRecyclerView()

        viewModel.getResults(showId, seasonNumber)

        viewModel.episodesList.observe(this){
            (binding.EpisodesRV.adapter as EpisodesListAdapter).submitList(it)
        }
    }

    private fun setupRecyclerView() = binding.EpisodesRV.apply {
        adapter = EpisodesListAdapter()
        layoutManager = LinearLayoutManager(this@EpisodesActivity)
    }
}