package com.example.moviesapp.ui.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.moviesapp.R
import com.example.moviesapp.data.model.ShowModel
import com.example.moviesapp.databinding.ShowListCardBinding
import com.example.moviesapp.ui.view.SeasonActivity

class ShowListAdapter : ListAdapter<ShowModel, ShowListAdapter.ShowListHolder>(ShowDiffCallback()) {

    inner class ShowListHolder(val binding: ShowListCardBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShowListHolder {
        return ShowListHolder(
            ShowListCardBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ShowListHolder, position: Int) {
        holder.binding.apply {
            val show = getItem(position)
            cardTitle.text = show.name
            val score = "Score: " + show.voteAverage.toString() + "/10"
            cardRating.text = score
            val thumbnailURL = "https://image.tmdb.org/t/p/w500" + show.posterPath
            Glide.with(cardLayout.context).load(thumbnailURL).placeholder(R.drawable.error)
                .into(Thumbnail)
            cardLayout.setOnClickListener {
                val intentDetails = Intent(cardLayout.context, SeasonActivity::class.java)
                intentDetails.putExtra("poster", show.posterPath)
                intentDetails.putExtra("title", show.name)
                intentDetails.putExtra("overView", show.overview)
                intentDetails.putExtra("id", show.id)

                ContextCompat.startActivity(cardLayout.context, intentDetails, null)
            }
        }
    }


}