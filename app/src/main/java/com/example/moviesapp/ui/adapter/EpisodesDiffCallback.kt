package com.example.moviesapp.ui.adapter

import androidx.recyclerview.widget.DiffUtil
import com.example.moviesapp.data.model.EpisodeModel

class EpisodesDiffCallback : DiffUtil.ItemCallback<EpisodeModel>() {
    override fun areItemsTheSame(oldItem: EpisodeModel, newItem: EpisodeModel): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: EpisodeModel, newItem: EpisodeModel): Boolean {
        return oldItem == newItem
    }
}