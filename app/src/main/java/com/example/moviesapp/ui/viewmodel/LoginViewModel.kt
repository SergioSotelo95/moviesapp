package com.example.moviesapp.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.moviesapp.domain.GetLoginSessionUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    application: Application,
    private val sessionUseCase: GetLoginSessionUseCase
) : AndroidViewModel(application) {

    enum class LogState {
        SUCCESS, ERROR, PENDING
    }

    val logState = MutableLiveData(LogState.PENDING)
    val sessionId = MutableLiveData<String>()

    fun getSession(username:String, password:String) {
        viewModelScope.launch {
            val login = sessionUseCase(username, password)
            if(login != null){
                if(login.success){
                    sessionId.postValue(login.session_id)
                    logState.postValue(LogState.SUCCESS)
                }else{
                    logState.postValue(LogState.ERROR)
                }
            }else{
                logState.postValue(LogState.ERROR)
            }

        }
    }

    fun resetLogState(){
        logState.postValue(LogState.PENDING)
    }

}