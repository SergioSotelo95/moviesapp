package com.example.moviesapp.ui.view

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import com.example.moviesapp.databinding.ActivityAuthBinding
import com.example.moviesapp.ui.viewmodel.LoginViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AuthActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAuthBinding
    private val viewModel: LoginViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAuthBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val username = binding.LoginUsername
        val password = binding.LoginPassword
        val intentPopular = Intent(binding.LoginButton.context, PopularActivity::class.java)


        binding.LoginButton.setOnClickListener {
            viewModel.getSession(username.text.toString(), password.text.toString())
        }

        binding.LoginRegisterButton.setOnClickListener {
            val i = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.themoviedb.org/signup"))
            startActivity(i)
        }



        viewModel.logState.observe(this){
            when (it) {
                LoginViewModel.LogState.SUCCESS -> {
                    intentPopular.putExtra("sessionId", viewModel.sessionId.value)
                    ContextCompat.startActivity(binding.LoginButton.context, intentPopular, null)

                }
                LoginViewModel.LogState.ERROR -> {
                    Toast.makeText(this, "Log In Error\nCheck username and/or password", Toast.LENGTH_SHORT).show()
                    viewModel.resetLogState()
                }
                else -> {
                    Toast.makeText(this, "Log In with username and password", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}