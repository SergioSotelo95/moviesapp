package com.example.moviesapp.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.moviesapp.data.model.EpisodeModel
import com.example.moviesapp.domain.GetEpisodesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class EpisodesViewModel @Inject constructor(
    application: Application,
    private val getEpisodesUseCase: GetEpisodesUseCase
) : AndroidViewModel(application) {
    val episodesList = MutableLiveData<List<EpisodeModel>>()

    fun getResults(id:Int, season: Int) {
        viewModelScope.launch {
            val result = getEpisodesUseCase(id, season)
            if (result.episodes.isNotEmpty()){
                episodesList.postValue(result.episodes)
            }
        }
    }

}