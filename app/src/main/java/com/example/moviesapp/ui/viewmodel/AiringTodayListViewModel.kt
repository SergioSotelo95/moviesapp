package com.example.moviesapp.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.moviesapp.data.model.ShowModel
import com.example.moviesapp.domain.GetAiringTodayUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AiringTodayListViewModel @Inject constructor(
    application: Application,
    private val getAiringTodayUseCase: GetAiringTodayUseCase
) : AndroidViewModel(application) {
    val showList = MutableLiveData<List<ShowModel>>()

    init {
        getResults()
    }

    private fun getResults() {
        viewModelScope.launch {
            val result = getAiringTodayUseCase()
            if (result.results.isNotEmpty()){
                showList.postValue(result.results)
            }
        }
    }

}