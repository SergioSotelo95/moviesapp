package com.example.moviesapp.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.moviesapp.data.model.ShowModel
import com.example.moviesapp.domain.GetTopRatedUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TopRatedListViewModel @Inject constructor(
    application: Application,
    private val getTopRatedUseCase: GetTopRatedUseCase
) : AndroidViewModel(application) {
    val showList = MutableLiveData<List<ShowModel>>()

    init {
        getResults()
    }

    private fun getResults() {
        viewModelScope.launch {
            val result = getTopRatedUseCase()
            if (result.results.isNotEmpty()){
                showList.postValue(result.results)
            }
        }
    }

}