package com.example.moviesapp.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.moviesapp.data.model.ShowModel
import com.example.moviesapp.data.model.authentication.UserModel
import com.example.moviesapp.domain.GetFavoritesUseCase
import com.example.moviesapp.domain.GetUserUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserViewModel @Inject constructor(
    application: Application,
    private val getUserUseCase: GetUserUseCase,
    private val getFavoritesUseCase: GetFavoritesUseCase
): AndroidViewModel(application) {
    val userInfo = MutableLiveData<UserModel>()
    val userFavorites = MutableLiveData<List<ShowModel>>()

    fun getUser(sessionId: String) {
        viewModelScope.launch {
            val user = getUserUseCase(sessionId)
            userInfo.postValue(user)
        }
    }

    fun getFavorites(sessionId: String){
        viewModelScope.launch {
            val favorites = getFavoritesUseCase(sessionId)
            if (favorites.results.isNotEmpty()){
                userFavorites.postValue(favorites.results)
            }
        }
    }

}