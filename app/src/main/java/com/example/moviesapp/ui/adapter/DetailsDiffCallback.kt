package com.example.moviesapp.ui.adapter

import androidx.recyclerview.widget.DiffUtil
import com.example.moviesapp.data.model.SeasonWithId

class DetailsDiffCallback : DiffUtil.ItemCallback<SeasonWithId>() {
    override fun areItemsTheSame(oldItem: SeasonWithId, newItem: SeasonWithId): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: SeasonWithId, newItem: SeasonWithId): Boolean {
        return oldItem == newItem
    }

}