package com.example.moviesapp.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.moviesapp.R
import com.example.moviesapp.data.model.EpisodeModel
import com.example.moviesapp.databinding.EpisodesListCardBinding

class EpisodesListAdapter :
    ListAdapter<EpisodeModel, EpisodesListAdapter.EpisodesListHolder>(EpisodesDiffCallback()) {

    inner class EpisodesListHolder(val binding: EpisodesListCardBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EpisodesListHolder {
        return EpisodesListHolder(
            EpisodesListCardBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: EpisodesListHolder, position: Int) {
        holder.binding.apply {
            val episode = getItem(position)
            val episodeNum = "Episode " + episode.episodeNumber.toString()
            EpisodeNumber.text = episodeNum
            EpisodeTitle.text = episode.name
            EpisodeRuntime.text = episode.overview
            val episodeImage = "https://image.tmdb.org/t/p/w500" + episode.stillPath
            Glide.with(EpisodesCardLayout.context).load(episodeImage).placeholder(R.drawable.error)
                .into(EpisodeThumbnail)
        }
    }


}