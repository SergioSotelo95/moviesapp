package com.example.moviesapp.ui.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.example.moviesapp.R
import com.example.moviesapp.databinding.ActivityLandingBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LandingActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLandingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_MoviesApp)
        super.onCreate(savedInstanceState)
        binding = ActivityLandingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.LandingButton.setOnClickListener {
            val intentAuth = Intent(binding.LandingButton.context, AuthActivity::class.java)
            ContextCompat.startActivity(binding.LandingButton.context, intentAuth, null)

        }
    }
}