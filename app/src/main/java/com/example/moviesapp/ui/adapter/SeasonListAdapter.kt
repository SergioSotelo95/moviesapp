package com.example.moviesapp.ui.adapter


import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.moviesapp.R
import com.example.moviesapp.data.model.SeasonWithId
import com.example.moviesapp.databinding.DetailListCardBinding
import com.example.moviesapp.ui.view.EpisodesActivity

class SeasonListAdapter : ListAdapter<SeasonWithId, SeasonListAdapter.SeasonListHolder>(DetailsDiffCallback()) {

    inner class SeasonListHolder(val binding: DetailListCardBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeasonListHolder {
        return SeasonListHolder(
            DetailListCardBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: SeasonListHolder, position: Int) {
        holder.binding.apply {
            val season = getItem(position)
            val seasonNumber = "Season " + season.seasonNumber.toString()
            Season.text = seasonNumber
            val episodeAmount = "Episodes: " + season.episodeCount.toString()
            SeasonEpisodes.text = episodeAmount
            SeasonSummary.text = season.overview
            val thumbnailURL = "https://image.tmdb.org/t/p/w500" + season.posterPath
            Glide.with(SeasonLayout.context).load(thumbnailURL).placeholder(R.drawable.error)
                .into(SeasonThumbnail)

            SeasonLayout.setOnClickListener {
                val intentEpisodes = Intent(SeasonLayout.context, EpisodesActivity::class.java)
                intentEpisodes.putExtra("season", season.seasonNumber)
                intentEpisodes.putExtra("showId", season.showId)
                ContextCompat.startActivity(SeasonLayout.context, intentEpisodes, null)
            }
        }
    }


}