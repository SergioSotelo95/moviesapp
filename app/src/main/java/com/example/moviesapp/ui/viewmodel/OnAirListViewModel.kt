package com.example.moviesapp.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.moviesapp.data.model.ShowModel
import com.example.moviesapp.domain.GetOnAirUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class OnAirListViewModel @Inject constructor(
    application: Application,
    private val getOnAirUseCase: GetOnAirUseCase
) : AndroidViewModel(application) {
    val showList = MutableLiveData<List<ShowModel>>()

    init {
        getResults()
    }

    private fun getResults() {
        viewModelScope.launch {
            val result = getOnAirUseCase()
            if (result.results.isNotEmpty()){
                showList.postValue(result.results)
            }
        }
    }

}